
package com.kenai.jaffl.provider;

public enum NativeType {
    VOID,
    SCHAR,
    UCHAR,
    SSHORT,
    USHORT,
    SINT,
    UINT,
    SLONG,
    ULONG,
    SLONGLONG,
    ULONGLONG,
    FLOAT,
    DOUBLE,
    ADDRESS;
}
