
package com.kenai.jaffl;

/**
 * Defines the function calling conventions.
 */
public enum CallingConvention {
    /**
     * The default C calling convention
     */
    DEFAULT,
    /**
     * Windows stdcall calling convention
     */
    STDCALL;
}
