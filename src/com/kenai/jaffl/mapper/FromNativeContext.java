
package com.kenai.jaffl.mapper;

/**
 * Context for a native->java type conversion.
 */
public interface FromNativeContext {
    // Nothing here, just a placeholder
}
