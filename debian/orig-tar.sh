#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>

DIR=jaffl-$2

tar xzf $3
mv wmeissner-* $DIR
tar cfz jaffl_$2.orig.tar.gz -X debian/orig-tar.exclude $DIR
mv jaffl_$2.orig.tar.gz ../

rm -rf $DIR
